<?php 
/*
 * Template Name: Landing Template
 */

get_header(); 

?>

<div class="book-cover">

<?php

	$description = get_bloginfo( 'description', 'display' );

	if ( $description || is_customize_preview() ) : ?>

	<div class="site-description">
	
		<p><b><?php echo current_time( 'F d, Y' ); ?></b></p>

		<h1>Welcome</h1>

		<p><?php echo $description; ?></p>
	</div>

<?php endif; ?>

<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/books-min.jpg" />

</div>

<?php get_sidebar( 'content-bottom' ); ?>

<?php get_footer(); ?>
